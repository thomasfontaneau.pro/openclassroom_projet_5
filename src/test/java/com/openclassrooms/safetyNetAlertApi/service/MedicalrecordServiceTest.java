package com.openclassrooms.safetyNetAlertApi.service;

import com.openclassrooms.safetyNetAlertApi.exceptions.PersonNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.repository.MedicalrecordRepository;
import com.openclassrooms.safetyNetAlertApi.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class MedicalrecordServiceTest {
  @Autowired
  private MedicalRecordService medicalRecordService;
  @MockBean
  private MedicalrecordRepository medicalrecordRepository;
  @MockBean
  private PersonRepository personRepository;

  private static final List<MedicalRecord> medicalRecordListTest = new ArrayList<>();

  private static final MedicalRecord medicalRecord = new MedicalRecord();

  @BeforeAll
  static void init() {
    MedicalRecord newMedicalRecord = new MedicalRecord();
    newMedicalRecord.setFirstName("Sophia");
    newMedicalRecord.setLastName("Zemicks");
    newMedicalRecord.setAllergies(List.of("aznol:60mg", "hydrapermazol:900mg", "pharmacol:5000mg", "terazine:500mg"));
    newMedicalRecord.setMedications(List.of("peanut", "shellfish", "aznol"));
    medicalRecordListTest.add(newMedicalRecord);
  }

  @Test
  public void getMedicalRecordsTest() {
    // GIVEN
    when(medicalrecordRepository.getMedicalRecords()).thenReturn(medicalRecordListTest);

    // WHEN
    List<MedicalRecord> medicalRecords = medicalRecordService.getMedicalRecords();

    // THEN
    assertEquals(medicalRecords.size(), 1);
  }

  @Test
  public void addMedicalRecordTest() {
    // GIVEN
    when(personRepository.getPerson("Warren", "Zemicks")).thenReturn(new Person());
    MedicalRecord newMedicalRecord = new MedicalRecord();
    newMedicalRecord.setFirstName("Warren");
    newMedicalRecord.setLastName("Zemicks");
    newMedicalRecord.setAllergies(List.of());
    newMedicalRecord.setMedications(List.of());

    // WHEN
    medicalRecordService.addMedicalRecords(newMedicalRecord);

    // THEN
    verify(medicalrecordRepository, times(1)).saveMedicalRecord(newMedicalRecord);
  }

  @Test
  public void deleteMedicalRecordTest() throws PersonNotFoundException {
    // GIVEN
    when(medicalrecordRepository.getMedicalRecord("Sophia", "Zemicks")).thenReturn(new MedicalRecord());

    // WHEN
    medicalRecordService.deleteMedicalRecord("Sophia","Zemicks");

    // THEN
    verify(medicalrecordRepository, times(1)).deleteMedicalRecord("Sophia","Zemicks");
  }

  @Test
  public void updateMedicalRecordTest() throws PersonNotFoundException {
    //GIVEN
    when(medicalrecordRepository.getMedicalRecord("Sophia", "Zemicks")).thenReturn(new MedicalRecord());
    MedicalRecord updateMedicalRecord = new MedicalRecord();
    updateMedicalRecord.setFirstName("Sophia");
    updateMedicalRecord.setLastName("Zemicks");
    updateMedicalRecord.setAllergies(List.of("aznol:60mg"));
    updateMedicalRecord.setMedications(List.of("peanut", "shellfish"));

    // WHEN
    medicalRecordService.updateMedicalRecord(updateMedicalRecord);

    // THEN
    verify(medicalrecordRepository, times(1)).updateMedicalRecord(updateMedicalRecord);
  }
}
