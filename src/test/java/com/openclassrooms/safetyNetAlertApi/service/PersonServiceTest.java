package com.openclassrooms.safetyNetAlertApi.service;

import com.openclassrooms.safetyNetAlertApi.exceptions.PersonNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.repository.PersonRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonServiceTest {
  @Autowired
  private PersonService personService;
  @MockBean
  private PersonRepository personRepository;

  private static final List<Person> personListTest = new ArrayList<>();

  private static final Person newPerson = new Person();

  @BeforeAll
  static void init() {
    newPerson.setFirstName("John");
    newPerson.setLastName("Boyd");
    newPerson.setAddress("st laurent");
    newPerson.setCity("Culver");
    newPerson.setZip(97451);
    newPerson.setPhone("841-874-6512");
    newPerson.setEmail("jaboy@email.com");
    personListTest.add(newPerson);
  }

  @Test
  public void getPersonsServiceTest() {
    // GIVEN
    when(personRepository.getPersons()).thenReturn(personListTest);

    // WHEN
    List<Person> persons = personService.getPersons();

    // THEN
    assertEquals(persons.size(), 1);
  }
  @Test
  public void addPersonServiceTest() {
    // GIVEN
    Person addPerson = new Person();
    addPerson.setFirstName("Tenley");
    addPerson.setLastName("Boyd");
    addPerson.setAddress("1509 Culver St");
    addPerson.setCity("Culver");
    addPerson.setZip(97451);
    addPerson.setPhone("841-874-6512");
    addPerson.setEmail("tenz@email.com");
    personListTest.add(addPerson);
    when(personRepository.getPerson("Tenley","Boyd")).thenReturn(null);
    when(personRepository.getPersons()).thenReturn(personListTest);

    // WHEN
    personService.addPerson(addPerson);

    // THEN
    verify(personRepository, times(1)).savePerson(addPerson);
  }
  @Test
  public void deletePersonServiceTest() throws PersonNotFoundException {
    // GIVEN
    when(personRepository.getPerson("John","Boyd")).thenReturn(newPerson);

    // WHEN
    personService.deletePerson("John","Boyd");

    // THEN
    verify(personRepository, times(1)).deletePerson("John","Boyd");
  }
  @Test
  public void updatePersonServiceTest() throws PersonNotFoundException {
    // GIVEN
    when(personRepository.getPerson("John","Boyd")).thenReturn(newPerson);
    newPerson.setEmail("test@email.com");

    // WHEN
    personService.updatePerson(newPerson);

    // THEN
    verify(personRepository, times(1)).updatePerson(newPerson);
  }
  @Test
  public void deletePersonServiceTestException() {
    try {
      // GIVEN
      when(personRepository.getPerson("John","Boyd")).thenReturn(null);

      // WHEN
      personService.deletePerson("John","Boyd");

    }catch(PersonNotFoundException e){
      // then
      assertTrue(e.getMessage().contains("Not person with the following information found"));
    }
  }
  @Test
  public void updatePersonServiceTestException() {

  }
}
