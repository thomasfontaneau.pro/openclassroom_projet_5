package com.openclassrooms.safetyNetAlertApi.service;

import com.openclassrooms.safetyNetAlertApi.exceptions.FirestationNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import com.openclassrooms.safetyNetAlertApi.repository.FirestationRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class FireStationServiceTest {
  @Autowired
  private FirestationService firestationService;
  @MockBean
  private FirestationRepository firestationRepository;

  static List<Firestation> fireStationListTest = new ArrayList<>();

  @BeforeAll
  static void init() {
    Firestation newFirestation = new Firestation();
    newFirestation.setAddress("509 Culver St");
    newFirestation.setStation(1);
    fireStationListTest.add(newFirestation);
  }

  @Test
  public void getFireStationsTest() {
    // GIVEN
    when(firestationService.getFirestations()).thenReturn(fireStationListTest);

    // WHEN
    List<Firestation> firestationList = firestationService.getFirestations();

    // THEN
    assertEquals(firestationList.size(), 1);
  }

  @Test
  public void addFireStationTest() {
    // GIVEN
    Firestation newFirestation = new Firestation();
    newFirestation.setAddress("5ft");
    newFirestation.setStation(1);

    // WHEN
    firestationService.addFirestation(newFirestation);

    // THEN
    verify(firestationRepository, times(1)).saveFirestation(newFirestation);
  }

  @Test
  public void deleteFireStationTest() throws FirestationNotFoundException {
    // GIVEN
    when(firestationRepository.getFirestation("509 Culver St")).thenReturn(new Firestation());

    // WHEN
    firestationService.deleteFirestation("509 Culver St");

    // THEN
    verify(firestationRepository, times(1)).deleteFirestation("509 Culver St");
  }

  @Test
  public void updateFireStationTest() throws FirestationNotFoundException {
    //GIVEN
    when(firestationRepository.getFirestation("509 Culver St")).thenReturn(new Firestation());
    Firestation updateFirestation = new Firestation();
    updateFirestation.setAddress("509 Culver St");
    updateFirestation.setStation(2);

    // WHEN
    firestationService.updateFirestation(updateFirestation);

    // THEN
    verify(firestationRepository, times(1)).updateFirestation(updateFirestation);
  }

  @Test
  public void getListPersonDependStationTest() throws FirestationNotFoundException {
    // WHEN
    firestationService.getListPersonDependStation(1);

    // THEN
    verify(firestationRepository, times(1)).getAddressesServed(1);
  }
}
