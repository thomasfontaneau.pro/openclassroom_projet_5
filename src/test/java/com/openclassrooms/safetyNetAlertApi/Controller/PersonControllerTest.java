package com.openclassrooms.safetyNetAlertApi.Controller;

import static org.hamcrest.CoreMatchers.is;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.safetyNetAlertApi.controller.PersonController;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.repository.PersonRepository;
import com.openclassrooms.safetyNetAlertApi.service.PersonService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonControllerTest {
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;
  @MockBean
  private PersonService personService;
  @MockBean
  private PersonRepository personRepository;

  private static final List<Person> personListTest = new ArrayList<>();

  @BeforeAll
  static void init() {
    Person newPerson = new Person();
    newPerson.setFirstName("John");
    newPerson.setLastName("Boyd");
    newPerson.setAddress("st laurent");
    newPerson.setCity("Culver");
    newPerson.setZip(97451);
    newPerson.setPhone("841-874-6512");
    newPerson.setEmail("jaboy@email.com");
    personListTest.add(newPerson);
  }

  @Test
  public void getPersonsTest() throws Exception {
    //GIVEN
    when(personService.getPersons()).thenReturn(personListTest);

    //THEN
    mockMvc.perform(get(PersonController.PATH)).andExpect(status().isOk()).andExpect(jsonPath("$[0].firstName", is("John")));
  }
  @Test
  public void addPersonTest() throws Exception {
    //GIVEN
    Person newPerson = new Person();
    newPerson.setFirstName("John");
    newPerson.setLastName("Boyd");
    newPerson.setAddress("150d9 Culver St");
    newPerson.setCity("Culver");
    newPerson.setZip(97451);
    newPerson.setPhone("841-874-6512");
    newPerson.setEmail("jaboy@email.com");
    doNothing().when(personService).addPerson(any());

    //THEN
    this.mockMvc.perform(MockMvcRequestBuilders.post(PersonController.PATH).content(objectMapper.writeValueAsString(newPerson)).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isCreated());
  }

  @Test
  public void deletePersonTest() throws Exception {
    //GIVEN
    Person person = new Person();
    person.setFirstName("John");
    person.setLastName("Boyd");
    person.setAddress("150d9 Culver St");
    person.setCity("Culver");
    person.setZip(97451);
    person.setPhone("841-874-6512");
    person.setEmail("jaboy@email.com");
    when(personRepository.getPerson("John","Boyd")).thenReturn(person);

    //THEN
    mockMvc.perform(MockMvcRequestBuilders.delete(PersonController.PATH+"?firstName=John&lastName=Boyd")).andExpect(status().isOk());
  }

  @Test
  public void updatePersonTest() throws Exception {
    //GIVEN
    Person updatePerson = new Person();
    updatePerson.setFirstName("John");
    updatePerson.setLastName("Boyd");
    updatePerson.setAddress("150d9 Culver St");
    updatePerson.setCity("Culver");
    updatePerson.setZip(97451);
    updatePerson.setPhone("841-874-6512");
    updatePerson.setEmail("jaboy@email.com");
    when(personRepository.getPerson("John","Boyd")).thenReturn(updatePerson);

    mockMvc.perform(MockMvcRequestBuilders.put(PersonController.PATH).content(objectMapper.writeValueAsString(updatePerson)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
  }
}
