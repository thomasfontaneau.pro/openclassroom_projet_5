package com.openclassrooms.safetyNetAlertApi.Controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TreatmentControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void getListPersonDependStationTest() throws Exception {
    mockMvc.perform(get("/childAlert").param("address", "1509 Culver St")).andExpect(status().isOk());
  }
  @Test
  public void getListPhoneAlertTest() throws Exception {
    mockMvc.perform(get("/phoneAlert?firestation=1")).andExpect(status().isOk());
  }

  @Test
  public void getFireTest() throws Exception {
    mockMvc.perform(get("/fire?address=1509 Culver St")).andExpect(status().isOk());
  }

  @Test
  public void getStationsTest() throws Exception {
    mockMvc.perform(get("/flood/stations?stations=1")).andExpect(status().isOk());
  }

  @Test
  public void getInhabitantTest() throws Exception {
    mockMvc.perform(get("/personInfo?firstName=Tenley&lastName=Boyd")).andExpect(status().isOk());
  }

  @Test
  public void getCommunityEmailTest() throws Exception {
    mockMvc.perform(get("/communityEmail?city=Culver")).andExpect(status().isOk());
  }
}
