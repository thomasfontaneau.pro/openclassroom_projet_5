package com.openclassrooms.safetyNetAlertApi.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.safetyNetAlertApi.controller.FirestationController;
import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import com.openclassrooms.safetyNetAlertApi.repository.FirestationRepository;
import com.openclassrooms.safetyNetAlertApi.service.FirestationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FireStationControllerTest {
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;
  @MockBean
  private FirestationService firestationService;
  @MockBean
  private FirestationRepository firestationRepository;

  static List<Firestation> fireStationListTest = new ArrayList<>();

  @BeforeAll
  static void init() {
    Firestation newFirestation = new Firestation();
    newFirestation.setAddress("509 Culver St");
    newFirestation.setStation(1);
    fireStationListTest.add(newFirestation);
  }

  @Test
  public void getFireStationTest() throws Exception {
    when(firestationRepository.getFirestations()).thenReturn(fireStationListTest);

    mockMvc.perform(MockMvcRequestBuilders.get(FirestationController.PATH)).andExpect(status().isOk());
  }

  @Test
  public void addFireStationTest() throws Exception {
    Firestation newFirestation = new Firestation();
    newFirestation.setAddress("5ft");
    newFirestation.setStation(1);

    this.mockMvc.perform(MockMvcRequestBuilders.post(FirestationController.PATH).content(objectMapper.writeValueAsString(newFirestation)).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isCreated());
  }

  @Test
  public void updateFireStationTest() throws Exception {
    Firestation updateFirestation = new Firestation();
    updateFirestation.setAddress("509 Culver St");
    updateFirestation.setStation(1);

    this.mockMvc.perform(MockMvcRequestBuilders.put(FirestationController.PATH).content(objectMapper.writeValueAsString(updateFirestation)).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk());
  }

  @Test
  public void deleteFireStationTest() throws Exception {

    when(firestationRepository.getFirestation("509 Culver St")).thenReturn(new Firestation());

    mockMvc.perform(MockMvcRequestBuilders.delete(FirestationController.PATH+"?address=509 Culver St")).andExpect(status().isOk());
  }

  @Test
  public void getListPersonDependStationTest() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(FirestationController.PATH+"?stationNumber=1")).andExpect(status().isOk());
  }
}
