package com.openclassrooms.safetyNetAlertApi.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.safetyNetAlertApi.controller.MedicalrecordController;
import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import com.openclassrooms.safetyNetAlertApi.repository.MedicalrecordRepository;
import com.openclassrooms.safetyNetAlertApi.service.MedicalRecordService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MedicalrecordControllerTest {
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;
  @MockBean
  private MedicalRecordService medicalRecordService;
  @MockBean
  private MedicalrecordRepository medicalrecordRepository;

  static List<MedicalRecord> medicalRecordListTest = new ArrayList<>();

  @BeforeAll
  static void init() {
    MedicalRecord newMedicalRecord = new MedicalRecord();
    newMedicalRecord.setFirstName("Sophia");
    newMedicalRecord.setLastName("Zemicks");
    newMedicalRecord.setAllergies(List.of("aznol:60mg", "hydrapermazol:900mg", "pharmacol:5000mg", "terazine:500mg"));
    newMedicalRecord.setMedications(List.of("peanut", "shellfish", "aznol"));
    medicalRecordListTest.add(newMedicalRecord);
  }

  @Test
  public void getMedicalRecordsTest() throws Exception {
    //GIVEN
    when(medicalRecordService.getMedicalRecords()).thenReturn(medicalRecordListTest);

    //THEN
    mockMvc.perform(MockMvcRequestBuilders.get(MedicalrecordController.PATH)).andExpect(status().isOk()).andExpect(jsonPath("$[0].firstName", is("Sophia")));
  }

  @Test
  public void addMedicalRecordsTest() throws Exception {
    //GIVEN
    MedicalRecord newMedicalRecord = new MedicalRecord();
    newMedicalRecord.setFirstName("Felicia");
    newMedicalRecord.setLastName("Boyd");
    newMedicalRecord.setAllergies(List.of("tetracyclaz:650mg"));
    newMedicalRecord.setMedications(List.of("xilliathal"));
    doNothing().when(medicalRecordService).addMedicalRecords(any());

    //THEN
    this.mockMvc.perform(MockMvcRequestBuilders.post(MedicalrecordController.PATH).content(objectMapper.writeValueAsString(newMedicalRecord)).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isCreated());
  }

  @Test
  public void deleteMedicalRecordsTest() throws Exception {
    //GIVEN
    MedicalRecord newMedicalRecord = new MedicalRecord();
    newMedicalRecord.setFirstName("Sophia");
    newMedicalRecord.setLastName("Zemicks");
    newMedicalRecord.setAllergies(List.of("aznol:60mg", "hydrapermazol:900mg", "pharmacol:5000mg", "terazine:500mg"));
    newMedicalRecord.setMedications(List.of("peanut", "shellfish", "aznol"));
    when(medicalrecordRepository.getMedicalRecord("Sophia","Zemicks")).thenReturn(newMedicalRecord);

    //THEN
    mockMvc.perform(MockMvcRequestBuilders.delete(MedicalrecordController.PATH+"?firstName=Sophia&lastName=Zemicks")).andExpect(status().isOk());
  }

  @Test
  public void updateMedicalRecordsTest() throws Exception {
    //GIVEN
    MedicalRecord updateMedicalRecord = new MedicalRecord();
    updateMedicalRecord.setFirstName("Sophia");
    updateMedicalRecord.setLastName("Zemicks");
    updateMedicalRecord.setAllergies(List.of("aznol:60mg", "hydrapermazol:900mg", "pharmacol:5000mg"));
    updateMedicalRecord.setMedications(List.of("peanut", "shellfish", "aznol"));
    when(medicalrecordRepository.getMedicalRecord("Sophia","Zemicks")).thenReturn(updateMedicalRecord);

    mockMvc.perform(MockMvcRequestBuilders.put(MedicalrecordController.PATH).content(objectMapper.writeValueAsString(updateMedicalRecord)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
  }
}
