package com.openclassrooms.safetyNetAlertApi.repository;

import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class MedicalrecordRepositoryTest {
  private static final MedicalrecordRepository medicalrecordRepository = new MedicalrecordRepository();

  private static final MedicalRecord newMedicalRecord = new MedicalRecord();

  @BeforeEach
  public void init() {
    medicalrecordRepository.cleanList();
    newMedicalRecord.setFirstName("Sophia");
    newMedicalRecord.setLastName("Zemicks");
    newMedicalRecord.setAllergies(List.of("aznol:60mg", "hydrapermazol:900mg", "pharmacol:5000mg", "terazine:500mg"));
    newMedicalRecord.setMedications(List.of("peanut", "shellfish", "aznol"));
    medicalrecordRepository.saveMedicalRecord(newMedicalRecord);
  }

  @Test
  public void getMedicalRecordsRepositoryTest() {
    List<MedicalRecord> medicalRecords = medicalrecordRepository.getMedicalRecords();
    assertEquals(medicalRecords.size(), 1);
    assertEquals(medicalRecords.get(0).getFirstName(), "Sophia");
  }

  @Test
  public void getMedicalRecordRepositoryTest() {
    MedicalRecord medicalRecord = medicalrecordRepository.getMedicalRecord("Sophia", "Zemicks");
    assertEquals(medicalRecord.getFirstName(), "Sophia");
  }

  @Test
  public void updateMedicalRecordRepositoryTest() {
    MedicalRecord updateMedicalRecord = new MedicalRecord();
    updateMedicalRecord.setFirstName("Sophia");
    updateMedicalRecord.setLastName("Zemicks");
    updateMedicalRecord.setAllergies(List.of("aznol:60mg"));
    updateMedicalRecord.setMedications(List.of("peanut", "shellfish"));

    medicalrecordRepository.updateMedicalRecord(updateMedicalRecord);
    MedicalRecord medicalRecord = medicalrecordRepository.getMedicalRecord("Sophia", "Zemicks");

    assertEquals(medicalRecord.getAllergies().size(), 1);
    assertEquals(medicalRecord.getMedications().size(), 2);
  }

  @Test
  public void addMedicalRecordRepositoryTest() {
    MedicalRecord newMedicalRecord = new MedicalRecord();
    newMedicalRecord.setFirstName("Warren");
    newMedicalRecord.setLastName("Zemicks");
    newMedicalRecord.setAllergies(List.of());
    newMedicalRecord.setMedications(List.of());

    medicalrecordRepository.saveMedicalRecord(newMedicalRecord);
    List<MedicalRecord> medicalRecords = medicalrecordRepository.getMedicalRecords();

    assertEquals(medicalRecords.size(), 2);
  }

  @Test
  public void deleteMedicalRecordRepositoryTest() {

    medicalrecordRepository.deleteMedicalRecord("Sophia", "Zemicks");
    List<MedicalRecord> medicalRecords = medicalrecordRepository.getMedicalRecords();

    assertEquals(medicalRecords.size(), 0);
  }
}
