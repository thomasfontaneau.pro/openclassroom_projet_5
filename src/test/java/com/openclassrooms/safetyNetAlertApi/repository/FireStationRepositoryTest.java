package com.openclassrooms.safetyNetAlertApi.repository;

import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class FireStationRepositoryTest {
  private static final FirestationRepository firestationRepository = new FirestationRepository();

  @BeforeEach
  public void init() {
    firestationRepository.cleanList();
    Firestation newFirestation = new Firestation();
    newFirestation.setAddress("509 Culver St");
    newFirestation.setStation(1);
    firestationRepository.saveFirestation(newFirestation);
  }

  @Test
  public void getFireStationsRepositoryTest() {
    List<Firestation> firestationList = firestationRepository.getFirestations();
    assertEquals(firestationList.size(), 1);
    assertEquals(firestationList.get(0).getAddress(), "509 Culver St");
  }

  @Test
  public void getFireStationRepositoryTest() {
    Firestation firestation = firestationRepository.getFirestation("509 Culver St");
    assertEquals(firestation.getAddress(), "509 Culver St");
  }

  @Test
  public void updateFireStationRepositoryTest() {
    Firestation updateFireStation = new Firestation();
    updateFireStation.setAddress("509 Culver St");
    updateFireStation.setStation(2);

    firestationRepository.updateFirestation(updateFireStation);
    Firestation firestation = firestationRepository.getFirestation("509 Culver St");

    assertEquals(firestation.getStation(), 2);
  }

  @Test
  public void addFireStationRepositoryTest() {
    Firestation newFirestation = new Firestation();
    newFirestation.setAddress("5ft");
    newFirestation.setStation(1);

    firestationRepository.saveFirestation(newFirestation);
    List<Firestation> firestationList = firestationRepository.getFirestations();

    assertEquals(firestationList.size(), 2);
  }

  @Test
  public void deleteFireStationRepositoryTest() {

    firestationRepository.deleteFirestation("509 Culver St");
    List<Firestation> firestationList = firestationRepository.getFirestations();

    assertEquals(firestationList.size(), 0);
  }
}
