package com.openclassrooms.safetyNetAlertApi.repository;

import com.openclassrooms.safetyNetAlertApi.model.Person;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonRepositoryTest {
  private static final PersonRepository personRepository = new PersonRepository();

  private static final Person newPerson = new Person();

  @BeforeEach
  public void init() {
    personRepository.cleanList();
    newPerson.setFirstName("John");
    newPerson.setLastName("Boyd");
    newPerson.setAddress("st laurent");
    newPerson.setCity("Culver");
    newPerson.setZip(97451);
    newPerson.setPhone("841-874-6512");
    newPerson.setEmail("jaboy@email.com");
    personRepository.savePerson(newPerson);
  }

  @Test
  public void getPersonsRepositoryTest() {
    List<Person> persons = personRepository.getPersons();
    assertEquals(persons.size(), 1);
    assertEquals(persons.get(0).getFirstName(), "John");
  }

  @Test
  public void getPersonRepositoryTest() {
    Person person = personRepository.getPerson("John", "Boyd");
    assertEquals(person.getFirstName(), "John");
    assertEquals(person.getLastName(), "Boyd");
  }

  @Test
  public void savePersonRepositoryTest() {
    Person personAdd = new Person();
    personAdd.setFirstName("Jonanathan");
    personAdd.setLastName("Marrack");
    personAdd.setAddress("29 15th St");
    personAdd.setCity("Culver");
    personAdd.setZip(97451);
    personAdd.setPhone("841-874-6513");
    personAdd.setEmail("drk@email.com");
    personRepository.savePerson(personAdd);

    List<Person> persons = personRepository.getPersons();


    assertEquals(persons.size(), 2);
    assertEquals(persons.get(1).getFirstName(), "Jonanathan");
  }

  @Test
  public void deletePersonRepositoryTest() {
    personRepository.deletePerson("John", "Boyd");
    List<Person> persons = personRepository.getPersons();

    assertEquals(persons.size(), 0);
  }

  @Test
  public void updatePersonRepositoryTest() {
    Person personUpdate = new Person();
    personUpdate.setFirstName("John");
    personUpdate.setLastName("Boyd");
    personUpdate.setAddress("68 15th St");
    personUpdate.setCity("Culver");
    personUpdate.setZip(97451);
    personUpdate.setPhone("841-874-6512");
    personUpdate.setEmail("jaboy@email.com");

    personRepository.updatePerson(personUpdate);
    List<Person> persons = personRepository.getPersons();

    assertEquals(persons.size(), 1);
    assertEquals(persons.get(0).getAddress(), "68 15th St");
  }
}
