package com.openclassrooms.safetyNetAlertApi.controller;

import com.openclassrooms.safetyNetAlertApi.exceptions.PersonNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import com.openclassrooms.safetyNetAlertApi.service.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(MedicalrecordController.PATH)
public class MedicalrecordController {
  public static final String PATH = "/medicalRecord";
  @Autowired
  private MedicalRecordService medicalRecordService;

  /**
   * Get all MedicalRecord
   */
  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  public List<MedicalRecord> getMedicalRecords() {
    return medicalRecordService.getMedicalRecords();
  }

  /**
   * Add a MedicalRecord
   */
  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public void addMedicalRecord(@RequestBody MedicalRecord newMedicalRecord) {
    medicalRecordService.addMedicalRecords(newMedicalRecord);
  }

  /**
   * Delete a MedicalRecord
   */
  @DeleteMapping
  @ResponseStatus(code = HttpStatus.OK)
  public void deleteMedicalRecord(@RequestParam String firstName, @RequestParam String lastName) throws PersonNotFoundException {
    medicalRecordService.deleteMedicalRecord(firstName, lastName);
  }

  /**
   * Update a MedicalRecord
   */
  @PutMapping
  @ResponseStatus(code = HttpStatus.OK)
  public void updateMedicalRecord(@RequestBody MedicalRecord medicalRecord) throws PersonNotFoundException {
    medicalRecordService.updateMedicalRecord(medicalRecord);
  }
}
