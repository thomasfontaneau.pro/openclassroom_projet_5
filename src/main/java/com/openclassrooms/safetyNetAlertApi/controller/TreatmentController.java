package com.openclassrooms.safetyNetAlertApi.controller;

import com.openclassrooms.safetyNetAlertApi.model.ChildAlert;
import com.openclassrooms.safetyNetAlertApi.model.Fire;
import com.openclassrooms.safetyNetAlertApi.model.Foyer;
import com.openclassrooms.safetyNetAlertApi.model.Inhabitant;
import com.openclassrooms.safetyNetAlertApi.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TreatmentController {

  @Autowired
  private TreatmentService treatmentService;

  @GetMapping(params = "address")
  @RequestMapping("/childAlert")
  public ChildAlert getListPersonDependStation(@RequestParam String address) {
    return treatmentService.getChildAlert(address);
  }

  @GetMapping(params = "firestation")
  @RequestMapping("/phoneAlert")
  public List<String> getListPhoneAlert(@RequestParam int firestation) {
    return treatmentService.getPhoneAlert(firestation);
  }

  @GetMapping(params = "address")
  @RequestMapping("/fire")
  public Fire getFire(@RequestParam String address) {
    return treatmentService.getFire(address);
  }

  @GetMapping(params = "stations")
  @RequestMapping("/flood/stations")
  public List<Foyer> getStations(@RequestParam int stations) {
    return treatmentService.getFoyers(stations);
  }

  @GetMapping()
  @RequestMapping("/personInfo")
  public List<Inhabitant> getInhabitant(@RequestParam String lastName) {
    return treatmentService.getInhabitant(lastName);
  }

  @GetMapping()
  @RequestMapping("/communityEmail")
  public List<String> getCommunityEmail(@RequestParam String city) {
    return treatmentService.getCommunityEmail(city);
  }
}
