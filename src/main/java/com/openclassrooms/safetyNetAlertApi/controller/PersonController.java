package com.openclassrooms.safetyNetAlertApi.controller;

import com.openclassrooms.safetyNetAlertApi.exceptions.PersonNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(PersonController.PATH)
public class PersonController {
  public static final String PATH = "/person";

  @Autowired
  private PersonService personService;

  /**
   * Get all persons
   */
  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  public List<Person> getPersons() {
    return personService.getPersons();
  }

  /**
   * Add a person
   */
  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public void addPerson(@RequestBody Person newPerson) {
    personService.addPerson(newPerson);
  }

  /**
   * Delete a person
   */
  @DeleteMapping
  @ResponseStatus(code = HttpStatus.OK)
  public void deletePerson(@RequestParam String firstName, @RequestParam String lastName) throws PersonNotFoundException {
    personService.deletePerson(firstName, lastName);
  }

  /**
   * Update a person
   */
  @PutMapping
  @ResponseStatus(code = HttpStatus.OK)
  public void updatePerson(@RequestBody Person updatePerson) throws PersonNotFoundException {
    personService.updatePerson(updatePerson);
  }
}
