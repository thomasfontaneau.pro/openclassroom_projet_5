package com.openclassrooms.safetyNetAlertApi.controller;

import com.openclassrooms.safetyNetAlertApi.exceptions.FirestationNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import com.openclassrooms.safetyNetAlertApi.model.PersonDependStation;
import com.openclassrooms.safetyNetAlertApi.service.FirestationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(FirestationController.PATH)
public class FirestationController {

  public static final String PATH = "/firestation";
  @Autowired
  private FirestationService firestationService;

  /**
   * Get all firestations
   */
  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  public List<Firestation> getFirestations() {
    return firestationService.getFirestations();
  }

  /**
   * Add a firestation
   */
  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public void addFirestation(@RequestBody Firestation newFirestation) {
    firestationService.addFirestation(newFirestation);
  }

  /**
   * Delete a firestation
   */
  @DeleteMapping
  @ResponseStatus(code = HttpStatus.OK)
  public void deleteFirestation(@RequestParam String address) throws FirestationNotFoundException {
    firestationService.deleteFirestation(address);
  }

  /**
   * Update a firestation
   */
  @PutMapping
  @ResponseStatus(code = HttpStatus.OK)
  public void updateFirestation(@RequestBody Firestation firestation) throws FirestationNotFoundException {
    firestationService.updateFirestation(firestation);
  }

  @GetMapping(params = "stationNumber")
  @ResponseStatus(code = HttpStatus.OK)
  public PersonDependStation getListPersonDependStation(@RequestParam int stationNumber) {
    return firestationService.getListPersonDependStation(stationNumber);
  }
}
