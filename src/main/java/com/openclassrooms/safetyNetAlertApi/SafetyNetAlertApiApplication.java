package com.openclassrooms.safetyNetAlertApi;

import com.openclassrooms.safetyNetAlertApi.utils.ReadFileUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SafetyNetAlertApiApplication implements CommandLineRunner {
  public static void main(String[] args) {
    SpringApplication.run(SafetyNetAlertApiApplication.class, args);
  }

  @Override
  public void run(String... args) {
    System.out.println("Import des Data");
    ReadFileUtils readFileUtils = new ReadFileUtils();
    readFileUtils.importData();
    System.out.println("Fin de l'import des Data");
  }
}
