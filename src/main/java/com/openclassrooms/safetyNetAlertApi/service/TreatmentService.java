package com.openclassrooms.safetyNetAlertApi.service;

import com.openclassrooms.safetyNetAlertApi.model.ChildAlert;
import com.openclassrooms.safetyNetAlertApi.model.Fire;
import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import com.openclassrooms.safetyNetAlertApi.model.Foyer;
import com.openclassrooms.safetyNetAlertApi.model.Inhabitant;
import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.repository.MedicalrecordRepository;
import com.openclassrooms.safetyNetAlertApi.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class TreatmentService {
  @Autowired
  private PersonService personService;
  @Autowired
  private MedicalrecordRepository medicalrecordRepository;
  @Autowired
  private FirestationService firestationService;

  public ChildAlert getChildAlert(String address) {
    List<Person> persons = personService.getPersons().stream().filter(person -> Objects.equals(person.getAddress(), address)).toList();
    List<Person> child = persons.stream().filter(person -> !DateUtils.isAdult(person.getBirthdate())).toList();
    List<Person> other = persons.stream().filter(person -> DateUtils.isAdult(person.getBirthdate())).toList();
    ChildAlert childAlert = new ChildAlert();
    childAlert.setChild(child);
    childAlert.setOther(other);
    return childAlert;
  }

  public List<String> getPhoneAlert(int firestationNumber) {
    List<String> address = firestationService.getFirestations().stream().filter(firestation -> Objects.equals(firestation.getStation(), firestationNumber)).map(Firestation::getAddress).toList();
    return personService.getPersons().stream().filter(person -> address.contains(person.getAddress())).map(Person::getPhone).toList();
  }

  public Fire getFire(String address) {
    String stationNumber = firestationService.getFirestations().stream().filter(firestation -> Objects.equals(firestation.getAddress(), address)).findFirst().map(Firestation::getStation).toString();
    List<Person> personList = personService.getPersons().stream().filter(person -> Objects.equals(person.getAddress(), address)).toList();
    HashMap<Person, MedicalRecord> personAndMedical = new HashMap<>();
    personList.forEach(person -> personAndMedical.put(person, medicalrecordRepository.getMedicalRecord(person.getFirstName(), person.getLastName())));
    Fire fire = new Fire();
    fire.setStationNumber(stationNumber);
    fire.setPersons(personAndMedical);
    return fire;
  }

  public List<Foyer> getFoyers(int stations) {
    List<Foyer> foyers = new ArrayList<>();
    List<Person> persons = firestationService.getListPersonDependStation(stations).getPersons();
    Map<String, List<Inhabitant>> personsByAddress = new HashMap<>();

    for (Person person : persons) {
      String address = person.getAddress();
      Inhabitant inhabitant = new Inhabitant();
      inhabitant.setInhabitant(person);
      inhabitant.setMedicalRecord(medicalrecordRepository.getMedicalRecord(person.getFirstName(), person.getLastName()));

      if (personsByAddress.containsKey(address)) {
        personsByAddress.get(address).add(inhabitant);
      } else {
        List<Inhabitant> listePersons = new ArrayList<>();
        listePersons.add(inhabitant);
        personsByAddress.put(address, listePersons);
      }
    }
    personsByAddress.forEach((address, inhabitants) -> foyers.add(new Foyer(address, inhabitants)));

    return foyers;
  }

  public List<Inhabitant> getInhabitant(String lastName) {
    List<Inhabitant> listInhabitant = new ArrayList<>();
    List<Person> listPerson = personService.getPersonsByLastname(lastName);
    listPerson.forEach(person -> {
              Inhabitant inhabitant = new Inhabitant();
              inhabitant.setInhabitant(person);
              inhabitant.setMedicalRecord(medicalrecordRepository.getMedicalRecord(person.getFirstName(), person.getLastName()));
              listInhabitant.add(inhabitant);
            });
    System.out.println(listPerson);
    return listInhabitant;
  }

  public List<String> getCommunityEmail(String city) {
    return personService.getPersons().stream().filter(person -> Objects.equals(person.getCity(), city)).map(Person::getPhone).toList();
  }
}
