package com.openclassrooms.safetyNetAlertApi.service;

import com.openclassrooms.safetyNetAlertApi.exceptions.FirestationNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.model.PersonDependStation;
import com.openclassrooms.safetyNetAlertApi.repository.FirestationRepository;
import com.openclassrooms.safetyNetAlertApi.repository.PersonRepository;
import com.openclassrooms.safetyNetAlertApi.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class FirestationService {
  @Autowired
  private FirestationRepository firestationRepository = new FirestationRepository();
  @Autowired
  private PersonRepository personRepository;

  public List<Firestation> getFirestations() {
    return firestationRepository.getFirestations();
  }

  public void addFirestation(Firestation firestation) {
    Assert.notNull(firestation, "");
    if (firestationRepository.getFirestation(firestation.getAddress()) == null)
      firestationRepository.saveFirestation(firestation);
  }

  public void deleteFirestation(String address) throws FirestationNotFoundException {
    try {
      if (firestationRepository.getFirestation(address) != null) {
        firestationRepository.deleteFirestation(address);
      }
    } catch (Exception e) {
      throw new FirestationNotFoundException(address);
    }
  }

  public void updateFirestation(Firestation firestation) throws FirestationNotFoundException {
    Assert.notNull(firestation, "");
    try {
      if (firestationRepository.getFirestation(firestation.getAddress()) != null) {
        firestationRepository.updateFirestation(firestation);
      }
    } catch (Exception e) {
      throw new FirestationNotFoundException(firestation.getAddress());
    }
  }

  public PersonDependStation getListPersonDependStation(int stationNumber) {
    List<String> addressesServed = firestationRepository.getAddressesServed(stationNumber);
    List<Person> personList = personRepository.getPersons();
    List<Person> filterPersonList = personList.stream().filter(person -> addressesServed.contains(person.getAddress())).toList();
    int nbChild = filterPersonList.stream().filter(person -> !DateUtils.isAdult(person.getBirthdate())).toList().size();
    int nbAdult = filterPersonList.stream().filter(person -> DateUtils.isAdult(person.getBirthdate())).toList().size();
    PersonDependStation personDependStation = new PersonDependStation();
    personDependStation.setPersons(filterPersonList);
    personDependStation.setNbChild(nbChild);
    personDependStation.setNbAdult(nbAdult);
    return personDependStation;
  }
}
