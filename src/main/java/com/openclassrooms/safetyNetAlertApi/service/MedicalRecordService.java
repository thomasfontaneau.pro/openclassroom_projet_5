package com.openclassrooms.safetyNetAlertApi.service;

import com.openclassrooms.safetyNetAlertApi.exceptions.PersonNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import com.openclassrooms.safetyNetAlertApi.repository.MedicalrecordRepository;
import com.openclassrooms.safetyNetAlertApi.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class MedicalRecordService {
  @Autowired
  private MedicalrecordRepository medicalrecordRepository = new MedicalrecordRepository();
  @Autowired
  private PersonRepository personRepository = new PersonRepository();

  public List<MedicalRecord> getMedicalRecords() {
    return medicalrecordRepository.getMedicalRecords();
  }

  public void addMedicalRecords(MedicalRecord medicalRecord) {
    Assert.notNull(medicalRecord, "");
    if (personRepository.getPerson(medicalRecord.getLastName(), medicalRecord.getFirstName()) == null)
      medicalrecordRepository.saveMedicalRecord(medicalRecord);
  }

  public void deleteMedicalRecord(String firstName, String lastName) throws PersonNotFoundException {
    try {
      if (medicalrecordRepository.getMedicalRecord(firstName, lastName) != null) {
        medicalrecordRepository.deleteMedicalRecord(firstName, lastName);
      }
    } catch (Exception e) {
      throw new PersonNotFoundException(firstName, lastName);
    }
  }

  public void updateMedicalRecord(MedicalRecord medicalRecordUpdate) throws PersonNotFoundException {
    Assert.notNull(medicalRecordUpdate, "");
    try {
      if (medicalrecordRepository.getMedicalRecord(medicalRecordUpdate.getFirstName(), medicalRecordUpdate.getLastName()) != null) {
        medicalrecordRepository.updateMedicalRecord(medicalRecordUpdate);
      }
    } catch (Exception e) {
      throw new PersonNotFoundException(medicalRecordUpdate.getFirstName(), medicalRecordUpdate.getLastName());
    }
  }
}
