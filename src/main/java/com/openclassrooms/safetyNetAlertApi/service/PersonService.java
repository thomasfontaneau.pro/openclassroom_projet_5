package com.openclassrooms.safetyNetAlertApi.service;

import com.openclassrooms.safetyNetAlertApi.exceptions.PersonNotFoundException;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class PersonService {
  @Autowired
  private PersonRepository personRepository = new PersonRepository();

  public List<Person> getPersons() {
    return personRepository.getPersons();
  }

  public void addPerson(Person person) {
    Assert.notNull(person, "");
    if (personRepository.getPerson(person.getLastName(), person.getFirstName()) == null)
      personRepository.savePerson(person);
  }

  public void deletePerson(String firstName, String lastName) throws PersonNotFoundException {
    try {
      if (personRepository.getPerson(firstName, lastName) != null) {
        personRepository.deletePerson(firstName, lastName);
      }
    } catch (Exception e) {
      throw new PersonNotFoundException(firstName, lastName);
    }
  }

  public void updatePerson(Person personUpdate) throws PersonNotFoundException {
    Assert.notNull(personUpdate, "");
    try {
      if (personRepository.getPerson(personUpdate.getFirstName(), personUpdate.getLastName()) != null) {
        personRepository.updatePerson(personUpdate);
      }
    } catch (Exception e) {
      throw new PersonNotFoundException(personUpdate.getFirstName(), personUpdate.getLastName());
    }
  }

  public void updateBirthdate(String firstName, String lastName, LocalDate birthdate) {
    personRepository.updateBirthdate(firstName, lastName, birthdate);
  }

  public List<Person> getPersonsByLastname(String lastName) {
    return personRepository.getPersonsByLastname(lastName);
  }
}
