package com.openclassrooms.safetyNetAlertApi.model;

import lombok.Data;

import java.util.List;

@Data
public class PersonDependStation {
  private List<Person> persons;
  private int nbChild;
  private int nbAdult;
}
