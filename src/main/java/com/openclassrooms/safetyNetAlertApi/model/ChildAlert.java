package com.openclassrooms.safetyNetAlertApi.model;

import lombok.Data;

import java.util.List;

@Data
public class ChildAlert {
  private List<Person> child;
  private List<Person> other;
}
