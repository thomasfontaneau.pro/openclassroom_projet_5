package com.openclassrooms.safetyNetAlertApi.model;

import lombok.Data;

import java.util.Map;

@Data
public class Fire {
  private String stationNumber;
  private Map<Person, MedicalRecord> persons;
}
