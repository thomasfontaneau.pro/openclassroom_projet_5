package com.openclassrooms.safetyNetAlertApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDate;

@Data
public class Person {
  @JsonProperty("firstName")
  private String firstName;
  @JsonProperty("lastName")
  private String lastName;
  private LocalDate birthdate;
  @JsonProperty("address")
  private String address;
  @JsonProperty("city")
  private String city;
  @JsonProperty("zip")
  private int zip;
  @JsonProperty("phone")
  private String phone;
  @JsonProperty("email")
  private String email;
}
