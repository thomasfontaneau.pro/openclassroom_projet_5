package com.openclassrooms.safetyNetAlertApi.model;

import lombok.Data;

@Data
public class Inhabitant {
  private Person inhabitant;
  private MedicalRecord medicalRecord;
}
