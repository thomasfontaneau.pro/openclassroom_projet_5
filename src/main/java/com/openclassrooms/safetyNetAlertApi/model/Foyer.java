package com.openclassrooms.safetyNetAlertApi.model;

import lombok.Data;

import java.util.List;

@Data
public class Foyer {
  private String address;
  private List<Inhabitant> inhabitants;

  public Foyer(String address, List<Inhabitant> inhabitant) {
    this.address = address;
    this.inhabitants = inhabitant;
  }
}
