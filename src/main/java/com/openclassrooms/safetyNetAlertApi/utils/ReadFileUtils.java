package com.openclassrooms.safetyNetAlertApi.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import com.openclassrooms.safetyNetAlertApi.model.Person;
import com.openclassrooms.safetyNetAlertApi.service.FirestationService;
import com.openclassrooms.safetyNetAlertApi.service.MedicalRecordService;
import com.openclassrooms.safetyNetAlertApi.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

@Service
public class ReadFileUtils {
  @Autowired
  private PersonService personService = new PersonService();
  @Autowired
  private FirestationService firestationService = new FirestationService();
  @Autowired
  private MedicalRecordService medicalRecordService = new MedicalRecordService();

  public void importData() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    try {
      File fichierJson = new File("src/main/resources/data.json");

      JsonNode rootNode = objectMapper.readTree(fichierJson);

      // Désérialisation des person
      JsonNode personsNode = rootNode.get("persons");
      if (personsNode.isArray()) {
        Iterator<JsonNode> personsIterator = personsNode.elements();
        while (personsIterator.hasNext()) {
          personService.addPerson(objectMapper.convertValue(personsIterator.next(), Person.class));
        }
      }

      // Désérialisation des firestation
      JsonNode firestationsNode = rootNode.get("firestations");
      if (firestationsNode.isArray()) {
        Iterator<JsonNode> firestationsIterator = firestationsNode.elements();
        while (firestationsIterator.hasNext()) {
          firestationService.addFirestation(objectMapper.convertValue(firestationsIterator.next(), Firestation.class));
        }
      }

      // Désérialisation des medicalrecord
      JsonNode medicalrecordsNode = rootNode.get("medicalrecords");
      if (medicalrecordsNode.isArray()) {
        Iterator<JsonNode> medicalrecordsIterator = medicalrecordsNode.elements();
        while (medicalrecordsIterator.hasNext()) {
          MedicalRecord medicalRecord = objectMapper.convertValue(medicalrecordsIterator.next(), MedicalRecord.class);
          medicalRecordService.addMedicalRecords(medicalRecord);
          personService.updateBirthdate(medicalRecord.getFirstName(), medicalRecord.getLastName(), LocalDate.parse(medicalRecord.getBirthdate(), formatter));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
