package com.openclassrooms.safetyNetAlertApi.utils;

import java.time.LocalDate;
import java.time.Period;

public class DateUtils {
  public static boolean isAdult(LocalDate birthdate) {
    if (birthdate == null) {
      return false;
    } else {
      return Period.between(birthdate, LocalDate.now()).getYears() >= 18;
    }
  }
}
