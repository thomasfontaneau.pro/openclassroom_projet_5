package com.openclassrooms.safetyNetAlertApi.exceptions;

public class PersonNotFoundException extends Throwable {
  public PersonNotFoundException(String firstName, String lastName) {
    super("Not person with the following information found : " + firstName + " " + lastName);
  }
}
