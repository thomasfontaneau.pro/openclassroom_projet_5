package com.openclassrooms.safetyNetAlertApi.exceptions;

public class FirestationNotFoundException extends Throwable {
  public FirestationNotFoundException(String address) {
    super("Not firestation with the following address found : " + address);
  }
}
