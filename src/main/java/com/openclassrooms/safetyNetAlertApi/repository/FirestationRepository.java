package com.openclassrooms.safetyNetAlertApi.repository;

import com.openclassrooms.safetyNetAlertApi.model.Firestation;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class FirestationRepository {
  static List<Firestation> firestationList = new ArrayList<>();

  public void saveFirestation(Firestation firestation) {
    firestationList.add(firestation);
  }

  public void deleteFirestation(String adresse) {
    for (Firestation firestation : firestationList) {
      if (Objects.equals(firestation.getAddress(), adresse)) {
        firestationList.remove(firestation);
        break;
      }
    }
  }

  public void updateFirestation(Firestation updateFirestation) {
    for (Firestation firestation : firestationList) {
      if (Objects.equals(firestation.getAddress(), updateFirestation.getAddress())) {
        firestation.setStation(updateFirestation.getStation());
        break;
      }
    }
  }

  public List<Firestation> getFirestations() {
    return firestationList;
  }

  public Firestation getFirestation(String address) {
    return firestationList.stream().filter(firestation ->
            Objects.equals(firestation.getAddress(), address)
    ).findFirst().orElse(null);
  }

  public List<String> getAddressesServed(int stationNumber) {
    return firestationList.stream().filter(firestation -> firestation.getStation() == stationNumber).map(Firestation::getAddress).toList();
  }

  public void cleanList() {
    firestationList.clear();
  }

}
