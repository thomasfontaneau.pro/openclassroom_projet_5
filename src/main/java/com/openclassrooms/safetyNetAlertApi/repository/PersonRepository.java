package com.openclassrooms.safetyNetAlertApi.repository;

import com.openclassrooms.safetyNetAlertApi.model.Person;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class PersonRepository {
  static List<Person> personList = new ArrayList<>();

  public void savePerson(Person person) {
    personList.add(person);
  }

  public void deletePerson(String firstName, String lastName) {
    for (Person person : personList) {
      if (Objects.equals(person.getLastName(), lastName) && Objects.equals(person.getFirstName(), firstName)) {
        personList.remove(person);
        break;
      }
    }
  }

  public void updatePerson(Person personUpdate) {
    for (Person person : personList) {
      if (Objects.equals(person.getLastName(), personUpdate.getLastName()) && Objects.equals(person.getFirstName(), personUpdate.getFirstName())) {
        person.setAddress(personUpdate.getAddress());
        person.setBirthdate(personUpdate.getBirthdate());
        person.setCity(personUpdate.getCity());
        person.setZip(personUpdate.getZip());
        person.setPhone(personUpdate.getPhone());
        person.setEmail(personUpdate.getEmail());
        break;
      }
    }
  }

  public Person getPerson(String firstName, String lastName) {
    return personList.stream().filter(person ->
            Objects.equals(person.getLastName(), lastName) && Objects.equals(person.getFirstName(), firstName)
    ).findFirst().orElse(null);
  }

  public void updateBirthdate(String firstName, String lastName, LocalDate birthdate) {
    for (Person person : personList) {
      if (Objects.equals(person.getLastName(), lastName) && Objects.equals(person.getFirstName(), firstName)) {
        person.setBirthdate(birthdate);
        break;
      }
    }
  }

  public List<Person> getPersons() {
    return personList;
  }

  public List<Person> getPersonsByLastname(String lastName) {
    return personList.stream().filter(person ->
            Objects.equals(person.getLastName(), lastName)
    ).toList();
  }

  public void cleanList() {
    personList.clear();
  }
}
