package com.openclassrooms.safetyNetAlertApi.repository;

import com.openclassrooms.safetyNetAlertApi.model.MedicalRecord;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class MedicalrecordRepository {
  static List<MedicalRecord> medicalRecordList = new ArrayList<>();

  public void saveMedicalRecord(MedicalRecord medicalRecord) {
    medicalRecordList.add(medicalRecord);
  }

  public void deleteMedicalRecord(String firstName, String lastName) {
    for (MedicalRecord medicalRecord : medicalRecordList) {
      if (Objects.equals(medicalRecord.getLastName(), lastName) && Objects.equals(medicalRecord.getFirstName(), firstName)) {
        medicalRecordList.remove(medicalRecord);
        break;
      }
    }
  }

  public void updateMedicalRecord(MedicalRecord updateMedicalRecord) {
    for (MedicalRecord medicalRecord : medicalRecordList) {
      if (Objects.equals(medicalRecord.getLastName(), updateMedicalRecord.getLastName()) && Objects.equals(medicalRecord.getFirstName(), updateMedicalRecord.getFirstName())) {
        medicalRecord.setMedications(updateMedicalRecord.getMedications());
        medicalRecord.setAllergies(updateMedicalRecord.getAllergies());
        break;
      }
    }
  }

  public MedicalRecord getMedicalRecord(String firstName, String lastName) {
    return medicalRecordList.stream().filter(medicalRecord ->
            Objects.equals(medicalRecord.getLastName(), lastName) && Objects.equals(medicalRecord.getFirstName(), firstName)
    ).findFirst().get();
  }

  public List<MedicalRecord> getMedicalRecords() {
    return medicalRecordList;
  }

  public void cleanList() {
    medicalRecordList.clear();
  }
}
